<?php
/**
 * importer.php
 * @author      Marc-André Appel <marcandre.appel@gmail.com>
 * @copyright   2016 Sudimage Communication
 * @license     http://opensource.org/licenses/MIT MIT
 * @link        http://www.sudimage.com
 */

$cwd = getcwd();

if (substr($cwd, -8) != 'incoming') {
    die('Not the right folder, please copy this file into ~/application/files/incoming');
} else {
    $incoming = dir($cwd);
    while ($lvl_1 = $incoming->read()) {
        if (strval(intval($lvl_1)) === "$lvl_1") {
            $lvl_1_root = dir($cwd . '/' . $lvl_1);
            while($lvl_2 = $lvl_1_root->read()) {
                if ($lvl_2 == 'index.html') {
                    print('Deleting ' . $cwd . '/' . $lvl_1 . '/' . $lvl_2 . "\n");
                    unlink($cwd . '/' . $lvl_1 . '/' . $lvl_2);
                }
                if (strval(intval($lvl_2)) === "$lvl_2") {
                    $lvl_2_root = dir($cwd . '/' . $lvl_1 . '/' . $lvl_2);
                    while ($lvl_3 = $lvl_2_root->read()) {
                        if ($lvl_3 == 'index.html') {
                            print('Deleting ' . $cwd . '/' . $lvl_1 . '/' . $lvl_2 . '/' . $lvl_3 . "\n");
                            unlink($cwd . '/' . $lvl_1 . '/' . $lvl_2 . '/' . $lvl_3);
                        }
                        if (strval(intval($lvl_3)) === "$lvl_3") {
                            $lvl_3_root = dir($cwd . '/' . $lvl_1 . '/' . $lvl_2 . '/' . $lvl_3);
                            while ($file = $lvl_3_root->read()) {
                                if ($file != '.' && $file != '..') {
                                    if ($file == 'index.html') {
                                        print('Deleting ' . $cwd . '/' . $lvl_1 . '/' . $lvl_2 . '/' . $lvl_3 . '/' . $file . "\n");
                                        unlink($cwd . '/' . $lvl_1 . '/' . $lvl_2 . '/' . $lvl_3 . '/' . $file);
                                    }
                                    print ('Moving ' . $cwd . '/' . $lvl_1 . '/' . $lvl_2 . '/' . $lvl_3 . '/' . $file . ' to ' . $cwd . '/' . $file . "\n");
                                    $filename = $file;
                                    if (file_exists($cwd . '/' . $file)) {
                                        $filename = rand(1000, 9999) . $file;
                                        print ('File exists already, renaming to ' . $filename);
                                    }
                                    rename($cwd . '/' . $lvl_1 . '/' . $lvl_2 . '/' . $lvl_3 . '/' . $file, $cwd . '/' . $filename);
                                }
                            }
                            print('Removing directory ' . $cwd . '/' . $lvl_1 . '/' . $lvl_2 . '/' . $lvl_3 . "\n");
                            rmdir($cwd . '/' . $lvl_1 . '/' . $lvl_2 . '/' . $lvl_3);
                        }
                    }
                    print('Removing directory ' . $cwd . '/' . $lvl_1 . '/' . $lvl_2 . "\n");
                    rmdir($cwd . '/' . $lvl_1 . '/' . $lvl_2);
                }
            }
            print('Removing directory ' . $cwd . '/' . $lvl_1 . "\n");
            rmdir($cwd . '/' . $lvl_1);
        }
    }
}