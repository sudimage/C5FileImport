# Concrete5 File Import Cleanup Script

Permits you to prepare the "incoming" folder after a rsync from another (older) 
Concrete5 installation _files_ folder. **Does not import the files into the C5 
file manager!** - You still need to go into the file manager and do it from the
"more options" popup.

## Using the import cleaner

Inside the ~/application/files/incoming folder do after the rsync:

    wget https://gitlab.com/sudimage/C5FileImport/raw/master/importer.php

Execute it:

    php importer.php
    
Cleanup leftover folders (thumbnails, avatars, etc are not taken into account!)
and import the files in Concrete5.